package com.ijpay.demo.controller.alipay;

import com.ijpay.alipay.AliPayApiConfig;

/**
 * @author Javen
 */
public abstract class AliPayApiController {
    public abstract AliPayApiConfig getApiConfig();
}
